import axios from "axios";
import { useState } from "react";
import WeddingTable from "./wedding-table";
import WeddingForm from './wedding-form';


export default function ViewerPage(){
    /* const weddings = [{weddingId: 10,
        weddingDate: 0,
        weddingLocation: "Spain",
        weddingName: "LaBoda",
        weddingBudget: 55000}] */

        const [weddings,setWeddings] = useState([])
        
    
        async function getWeddings(event){
            const response = await axios.get('http://34.73.107.128:3001/weddings/');
            setWeddings(response.data)
        }

    return(<div>
        <h1>Wedding Planner</h1>
        <button onClick={getWeddings}>List All Weddings</button>
        <WeddingTable weddings={weddings}></WeddingTable>
        <WeddingForm></WeddingForm>
    </div>

    )
}