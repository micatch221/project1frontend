import React, { useState } from "react";
import FileBase64 from 'react-file-base64';

export default function FileUpload(){

    return(
        <FileBase64 type="file" multiple={false}
        onDone={({base64}) => setListingData({ ...listingData, selectedFile: base64})}
        />
    )
}