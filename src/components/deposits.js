import * as React from 'react';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import Title from './title';

function preventDefault(event) {
  event.preventDefault();
}

export default function Deposits() {
  return (
    <React.Fragment>
      <Title>Recent Messages</Title>
      <Typography component="p" variant="h5">
        We need to add expenses to the Test wedding.
      </Typography>
      <Typography color="text.secondary" sx={{ flex: 1 }}>
        sender: sender@wed.com
      </Typography>
      <div>
        <Link color="primary" href="#" onClick={preventDefault}>
          See More Messages
        </Link>
      </div>
    </React.Fragment>
  );
}