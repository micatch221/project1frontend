import * as React from 'react';
import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Title from './title.js';
import axios from 'axios';



export default function Orders() {

  const [rows,setWeddings] = React.useState([])

  async function getWeddings(event){
    const response = await axios.get('http://34.73.107.128:3001/weddings/');
    setWeddings(response.data)
        }


  return (
    <React.Fragment>
      <Title>Wedding List</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell>Date</TableCell>
            <TableCell>Location</TableCell>
            <TableCell>Name</TableCell>
            <TableCell align="right">Budget</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.weddingId}>
              <TableCell>{row.weddingId}</TableCell>
              <TableCell>{row.weddingDate}</TableCell>
              <TableCell>{row.weddingLocation}</TableCell>
              <TableCell>{row.weddingName}</TableCell>
              <TableCell align="right">{`$${row.weddingBudget}`}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Link color="primary" href="#" onClick={getWeddings} sx={{ mt: 3 }}>
        See more weddings
      </Link>
    </React.Fragment>
  );
}