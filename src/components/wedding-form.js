import axios from "axios";
import { useRef } from "react"
import Title from './title.js';


export default function WeddingForm(){

    const dateInput = useRef(null);
    const locationInput = useRef(null);
    const nameInput = useRef(null);
    const budgetInput = useRef(null);

    async function addWedding(){

    //The getTime() method returns the number of milliseconds 
    //from midnight of January 1, 1970 (EcmaScript epoch) to the specified date.
    //Convert the date to a number to store in the database
        
        const wedding = {
            weddingId:0,
            weddingDate:Date.parse(dateInput.current.value),
            weddingLocation:locationInput.current.value,
            weddingName:nameInput.current.value,
            weddingBudget:Number(budgetInput.current.value)
        }
        console.log(wedding)
        const response = await axios.post('http://34.73.107.128:3001/weddings',wedding)
        console.log(response)
    }

    return(<div>

        <Title>Wedding Form</Title>
        
        <input placeholder="date" ref={dateInput} type="date"></input>
      
        <input placeholder="location" ref={locationInput}></input>
        <input placeholder="name" ref={nameInput}></input>
        <input placeholder="budget" ref={budgetInput} type="number"></input>
        <button onClick={addWedding}>Add Wedding</button>

    </div>)
}