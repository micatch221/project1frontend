
import React, { useState } from "react";
import axios from "axios";
import FileBase64 from 'react-file-base64';


function App() {
  const [baseImage, setBaseImage] = useState("");

  const uploadImage = async (e) => {
    const file = e.target.files[0];
    const base64 = await convertBase64(file);
    setBaseImage(base64);
    const base64result = base64.substr(base64.indexOf(',')+1)
    const fileName = file.name;
    const fileBody = {name:fileName,content:base64result}
    console.log(fileBody)
    const response = await axios.post('http://localhost:8080/',fileBody)
    console.log(response)
  };

 

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = () => {
        resolve(fileReader.result);
      };

      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };

  return (
    <div className="App">
      <input
        type="file"
        onChange={(e) => {
          uploadImage(e);
        }}
      />
      <br></br>
      <img src={baseImage} height="200px" />
    </div>
  );
}

export default App;